
#ifndef _RVEX_DIRENT_H
#define _RVEX_DIRENT_H

#include <sys/stat.h>

struct dirent {
    ino_t d_ino;
    char  d_name[16]; // Quake just uses 8.3 filenames anyway
};

typedef struct dirent* DIR;

// Quake only uses the following from dirent.h:
DIR *opendir(const char *fname);
struct dirent *readdir(DIR *dir);
int closedir(DIR *dir);

#endif
