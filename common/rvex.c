
#include <machine/rvex-sys.h>
#include <machine/rvex.h>
#include <sys/errno.h>

#define UART_DATA (*((volatile unsigned char *)(0xD1000000)))
#define UART_STAT (*((volatile unsigned char *)(0xD1000004)))
#define UART_CTRL (*((volatile unsigned char *)(0xD1000008)))

void
sys_puts(const char *s)
{
    while (*s)
    {
	
	// Stop if the TX data FIFO ready flag is not set.
	if (!(UART_STAT & (1 << 1))) {
	    break;
	}
	
	// Write the next character.
	UART_DATA = *s++;
	
    }
}

rvex_sysreturn_t
sys_write(int fd, void *buf, size_t cnt)
{
    rvex_sysreturn_t ret;
    
    // Handle stdout and stderr.
    if ((fd == 1) || (fd == 2))
    {
	if (fd == 1) {
	    sys_puts("<OUT>");
	} else {
	    sys_puts("<ERR>");
	}
	
	ret.retval = cnt;
	ret.err_no = 0;
	
	const char *cbuf = (const char*)buf;
	while (cnt)
	{
	    
	    // Stop if the TX data FIFO ready flag is not set.
	    if (!(UART_STAT & (1 << 1))) {
		break;
	    }
	    
	    // Write the next character.
	    UART_DATA = *cbuf++;
	    cnt -= 1;
	    
	}
	
	return ret;
    }
    
    // Handle stdin.
    if (fd == 0)
    {
	ret.retval = -1;
	ret.err_no = EINVAL;
	return ret;
    }
    
    // TODO: handle normal files.
    
    // Bad file descriptor.
    ret.retval = -1;
    ret.err_no = EBADF;
    return ret;
}

