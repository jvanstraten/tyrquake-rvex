
#include "net.h"
#include "net_dgrm.h"
#include "net_loop.h"
#include "net_udp.h"

net_driver_t net_drivers[] = {
    {
	.name				= "Loopback",
	.initialized			= false,
	.Init				= Loop_Init,
	.Listen				= Loop_Listen,
	.SearchForHosts			= Loop_SearchForHosts,
	.Connect			= Loop_Connect,
	.CheckNewConnections		= Loop_CheckNewConnections,
	.QGetMessage			= Loop_GetMessage,
	.QSendMessage			= Loop_SendMessage,
	.SendUnreliableMessage		= Loop_SendUnreliableMessage,
	.CanSendMessage			= Loop_CanSendMessage,
	.CanSendUnreliableMessage	= Loop_CanSendUnreliableMessage,
	.Close				= Loop_Close,
	.Shutdown			= Loop_Shutdown
    }
};

int net_numdrivers = 1;

net_landriver_t net_landrivers[1];

int net_numlandrivers = 0;

void
NET_Ban_f(client_t *client)
{
}

qboolean isDedicated = false;


/*qboolean tcpipAvailable = false;
char my_tcpip_address[NET_NAMELEN] = "0.0.0.0";

qboolean slistInProgress = false;
qboolean slistSilent = false;
qboolean slistLocal = false;

qboolean isDedicated = false;

int DEFAULTnet_hostport = 26000;
int net_hostport;
sizebuf_t net_message;
double net_time = 0;
int net_activeconnections = 0;
int hostCacheCount = 0;
hostcache_t hostcache[HOSTCACHESIZE];
cvar_t hostname = { "hostname", "UNNAMED" };

void
NET_Ban_f(client_t *client)
{
}

qboolean
NET_CanSendMessage(qsocket_t *sock)
{
    return false;
}

double
SetNetTime(void)
{
    net_time = Sys_DoubleTime();
    return net_time;
}

qsocket_t *
NET_CheckNewConnections(void)
{
    SetNetTime();
    return NULL;
}

void
NET_Close(qsocket_t *sock)
{
}

qsocket_t *
NET_Connect(const char *host)
{
    qsocket_t *ret;
    int i, n;
    int numdrivers = net_numdrivers;

    SetNetTime();

    if (host && *host == 0)
	host = NULL;

    if (host) {
	if (strcasecmp(host, "local") == 0) {
	    numdrivers = 1;
	    goto JustDoIt;
	}

	if (hostCacheCount) {
	    for (n = 0; n < hostCacheCount; n++)
		if (strcasecmp(host, hostcache[n].name) == 0) {
		    host = hostcache[n].cname;
		    break;
		}
	    if (n < hostCacheCount)
		goto JustDoIt;
	}
    }

    slistSilent = host ? true : false;
    NET_Slist_f();

    while (slistInProgress)
	NET_Poll();

    if (host == NULL) {
	if (hostCacheCount != 1)
	    return NULL;
	host = hostcache[0].cname;
	Con_Printf("Connecting to...\n%s @ %s\n\n", hostcache[0].name, host);
    }

    if (hostCacheCount)
	for (n = 0; n < hostCacheCount; n++)
	    if (strcasecmp(host, hostcache[n].name) == 0) {
		host = hostcache[n].cname;
		break;
	    }

  JustDoIt:
    for (i = 0; i < numdrivers; i++) {
	net_driver = &net_drivers[i];
	if (net_driver->initialized == false)
	    continue;
	ret = net_driver->Connect(host);
	if (ret)
	    return ret;
    }

    if (host) {
	Con_Printf("\n");
	PrintSlistHeader();
	PrintSlist();
	PrintSlistTrailer();
    }

    return NULL;
}

int
NET_GetMessage(qsocket_t *sock)
{
    return -1;
}

void
NET_Init(void)
{
    SZ_Alloc(&net_message, NET_MAXMESSAGE);
}

void
NET_Poll(void)
{
    SetNetTime();
}

int
NET_SendMessage(qsocket_t *sock, const sizebuf_t *data)
{
    return -1;
}

int
NET_SendUnreliableMessage(qsocket_t *sock, const sizebuf_t *data)
{
    return -1;
}

void
NET_Shutdown(void)
{
}

void
NET_Slist_f(void)
{
}

int
NET_SendToAll(const sizebuf_t *data, double blocktime)
{
    return 0;
}
*/
